# Development Notes

## 2025-03-02

- Moving back to a subdirectory structure to make sure that works fine.
- OK, and moving the colony state outside the file to separate concerns.
- I can't do the same with the display stuff right now...since that directly uses the colony state, I need to create the overriding app that sits *above* the `textual` app first. That's too much of a stretch right now. But I can move almost everything into an `app.py` and run from there.
- I think the only thing I need from my previous attempt are the star name stuff, since that took a while and I don't want to go through it again.
- And back in an installable codebase! Play with `python -m arcturus`.
- I *had* been thinking about this being turn-based, but maybe real-time would be more fun? In that case, here's what we can do next:
  - Set up a button for resupply
  - Have a countdown timer for when the resupply shows up
  - Deactivate the button while waiting for resupply
- I've moved it to a `Screen` so that I can multiple screen that I can switch between. It's overkill right now, but will be helpful later.
- I don't have the resupply on a countdown...time to figure that out.
- After going back and forth for a while, I have a condensed display that I *should* be able to reuse pretty easily!

## 2025-02-27

- OK, basics. Our game is driven by the interaction of the player with the system, and we need to start from there.
- Start from the display layer, and abstract things out into an engine as needed, instead of doing the reverse. Build up functionality slowly.
- And let's even start with sub-apps within the full game. Things like requesting supplies would be a part of the "Administration" view. Can we just build that with some placeholders?
- Am I just bad at thinking about this game? I'm blanking on normal interactions. OK, how about this?
  - We have a grid of colony modules. They all start empty. We can click on them, and get a new module from an array.
  - We can represent those modules with a single letter and change the color.
  - Clicking repeatedly will cycle through, eventually getting back to empty
- OK, I did that. It doesn't look good, but it does work.
- OK, a quick change. Instead of focusing on multiple modules, let's make sure one works. We'll build something that can have generic workers assigned or removed from it.
- HAHA! And I have a basic colony state involved. I can limit how many workers get allocated based on a total that is maintained outside of the widgets.
- That *feels* like a big breakthrough. I have a state that describes the colony, and interactions that can adjust that state. Let's pause here for now.

## 2025-02-23

- Thinking about Manor Lords, it's rare that you build more than one of a building outside of some of the basic resource collection ones and the residential ones. You *might* build a second building if you have a massive production chain and your entire city is focused on just that. Farms fit into this, if only because of the massive delay in getting resources.
- The specialization comes from which buildings you build, plus some of the augmentations on the residential spaces.
- With that in mind, we should be clear what the administration controls and what the colonist controls.
- We also need to build the interactions of the *player* with the engine and modules. So, the first few steps in the basic game loop below that we haven't created yet.
- For focus, let's build "request a commerical resupply mission"
  - We'll want to eventually build a "schedule resupply" kind of like Manor Lord's trading, but we'll do that later.
- Administrative action, selecting from a present set of supplies
- We have `LIFE_SUPPORT` as a resource, which we'll eventually split into a bunch of resources. For now, let's have this be the resource we bring up.
  - Aside: we probably should remove "housing" from resources and treat that differently
- You know what? Maybe I need to go through the `textual` tutorial first...kind of like I did the `typer` tutorial first. Let's do that.
- While doing that, Laura gave me the great idea to have dogs *and* colonists! I think that's excellent. But I need to get to that point first.

## 2025-02-17

- Let's get our basic game loop set up:
  - Take actions to assign colonists to jobs
  - Take coloniy management actions (e.g., request supply delivery)
  - Submit work orders
  - End-of-turn processing: consume power + life support, generate research
  - Repeat
- OK, workspaces from the colony are associated to the colonist at the colonist level. Each "month", the colony consumes resources, the colony produces resources, the colonists consume resources, and the colonists produce resources. Some resources can't be stored unless you have a module that can store it. So we need to move our storage from associated to the *colony* and associated to a *module*.
- I also need to rethink power and life support too. We can't store excess power *unless* we have a battery bank. What should massive overproduction of power do for the colony? Likewise, life support is just *there*, and we don't really have a consequence if we don't have enough life support.
- Is this just telling me to figure that stuff out later? Maybe.

## 2025-02-16

- I have the basic models, file saving, start-up, etc. created! Sure, every time we run the script we create a new colony, but that's fine for now.
- I'm tentatively calling this **Arcturus** to avoid the word "colony" becoming meaningless.
- At this point, I'm ready to start populating details in the app!
- And we're good! Just bare-bones display, no styling, but it's there.
- Now we need to figure out what the game actually looks like. Let's focus on the interactions that the player can do:
  - Build/activate/disable module
  - Assign colonist to job, or remove from job
  - Request supplies
- Plus, the actual display of our entities...
- I distracted myself by getting star names. Oh well.
- OK, saving progress and going off the computer to think about what the game should actually look like.
- Welp, broke the display. This is where it's going to take a while to get it working. But I have `produce()` and `consume()` methods on my colonists and modules, which should help!

## 2025-02-15

- OK, Manor Lords and Diplomacy are having a larger impact on me. We can do pretty well with abstracted production and supply, plus resource chains, and step timing.
- Here's the plan: each tick moves forward step-wise in time, with actions taking some amount of ticks to complete
- You need jobs, power, water, etc. to get the benefits (not passive)
- You'll never have enough people, so you might need to pick and choose or rotate people around
- You start with basics: colony pod. You build modules attached to it to survive.
- You start with a small collection of resources
- You have policies that can affect your colony
- You can research to unlock new things
- Some resource deposits can run out, while others might not if upgrade or technology available
- OK, without getting too in the weeds, let's just start and make some choices
  - Populate some files describing what I might want to do
  - Bring in `pydantic` to manage the models themselves
  - Create a basic app and game file
- The game kind of goes hand-in-hand with the display, so it doesn't make much sense to work on them separately. I'm going to create a basic loop that will ensure that
  - Workers can be assigned to modules
  - Population consumes resources
  - Update mechanism works

## 2024-12-29

- Let's think about one basic pattern: food for people
- A farmer will cultivate food. Let's call them botanists that run hydroponics bays

## 2024-12-07

- Coming back to this to build something fun
- Going back to basics, let's write out what we think works and doesn't work in other systems.
- I started over-enginerring things again...let's create the Outpost 2 system to start.
  - Found the original manual with unit references: https://www.sierrachest.com/gfx/games/Outpost2/box/OUTPOST2.PDF
- And it's just a bunch of random stuff to add in
- I think I need to spend time just writing down what I care about. I want something closer to Stellaris as strategy, as the other ones are simplified a lot to support for RTS/TBS. So, let's think about specific roles, resources, and outcomes first.

## 2024-05-18

- Create repository and image (using Bing image creator)
- Create virtual environment for development
- Install and check that things work from the command line
- Start creating the `Colony` model with tests
- Create food consumption by population, and population birth from excess food. Both computations are very basic (each population eats one food) to get the structure in place. There definitely should be some considerations here, but this seems fine for now.
- Do some quick local tests on population growth

```python
from colony import model

my_colony = model.Colony(population=100, food=1000)

while my_colony.population > 0:
    my_colony.update()
    print(my_colony.current_step, my_colony.to_json())
    if c.population >= 200:
        break
```

- Create a CLI testing script, since it can take arguments now
- Include `farm` to grow food and test that it works as expected
- Create a new `Game` class that will handle meta-updating and tracking. Refactor to have this object be the top-level entry point into handling the objects.

## 2024-05-20

- Create `models` directory to handle all of our objects
- Create `Farm` object with a production rate property
- Add unique identifiers to all objects
- Update all objects to have a `json` property instead of a `to_json()` method
