# Resources

Items required for day-to-day operations and living within the colony.

*Note:* start with basics and add new resources over time.

## Water

- Necessary for life support, growing crops, keeping people alive
- Can be recycled at some efficiency (upgradeable)
- Collected by Vapor Condensors
- Can be split into Oxygen and Hydrogen

## Air / Oxygen

- Oxygen, Nitrogen, Carbon Dioxide
- Base requirement for all colonists
- Stored in pressurized containers

## Food

- Carbohydrates: grain
- Protein: synthetic beef and fish

## Medicine

- Treats sick or injured colonists

## Luxury goods

- Digital: music, entertainment, books
- Physical: jewelry, alcohol, toys

## Clothing

- Needs to be continually replaced

## Metals

### Common metals

- Aluminium, Silicon, Copper, Iron
- Mined and refined by Mineral Processing Facility
- Required for basic structures, repairs, maintenance

### Rare metals

- Titanium, Gold, Lithium
- Mined and refined by Mineral Processing Facility after an upgrade
- Required for advanced structures, repairs, maintenance

## Power

- Electricity produced by power generating facilities
- Passive power requirements for colony to function. Not enough power? Random individual brownouts and reduced production everywhere.
- Can be stored in Batteries during surplus production

## Fuel

- Hydrogen: fuel cells
