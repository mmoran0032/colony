# Design exploration

Rough chat discussion on other systems.

## Stellaris

- Planets have generic districts and special buildings
- Each population is generalized but has a role
- Each role produces some output
- Multiple physical and non-physical goods
- Population growth based on available housing and food (and probably some other things)

## Alpha Centauri

- Bases have enhancements that adjust colony performance or benefits
- Population works on tiles or as generalized researchers
- Local resources determine growth and capabilities
- Population split between drone, empath, etc. (effectively worker + scientist)

## Outpost 2

- Population split between ages
- Population serves a specific role within the colony
- Specialization split between worker and scientist
- Resources:
  - Food: produced by Agridomes, stored by Agridomes, consumed by population
  - Common/rare ore: produced by mines, consumed by smelters
  - Common/rare metals: produced by smelters, stored by smelters, consumed by buildings
  - Power: produced by power stations, consumed by buildings
  - Morale: affects all production, birth/death
- Population:
  - Children: consume food
  - Workers: consume food, work basic jobs
  - Scientists: consume food, work basic and advanced jobs
- Children created by nursery, children turned to workers by university, workers can be trained to be scientists by university
- https://wiki.outpost2.net/doku.php?id=outpost_2:concepts:population_and_morale contains calculations

## Manor Lords

- Relatively new, but applicable
- Agentic
- Every person needs food, and also wants a variety of food
- People can have jobs, which are at specific building, which allow the building to function
- Buildings need resources that either come from nature or another building
- Supply chain management
- Each action takes a certain amount of time

## Race for the Galaxy

- Abstract supply chain
- Unique developments and locations
- Different types of goods
- Goods stay around until used
