# Colony Modules

Available modules to build within your colony. Each module has a number of open jobs. When that job is staffed, the module provides the resource based on who is staffed. Some resources take time to acquire, so transient resources will be built up until the full resource is developed.

*Note:* start with basic buildings and think about upgrade trees after. Essentially, see if you can "recreate" the experience of managing the ISS.

Basics:

- Command Center / Colonial Post
- Residence / Barracks
- Laboratory
- Hydroponics Bay
- Solar Array

## Command Center

Central authority of the colony

- Cost: Free. Initial colony starts with just this module.
- Provides: 4 housing, 4 power
- Jobs: 1 administrative, 1 security

*Note:* just start with the basic building and think about an upgrade tree after.

- Level 0: Prefabricated Shelter
- Level 1: Colonial Post
- Level 2: Colony Administration
- Level 3: Administrative Nexus

## Vapor Condenser

Collects water vapor from the ambient atmosphere

- Cost: X common metals
- Provides: X water
- Jobs: 0

## Battery Bank

Stores excess power

- Cost: X common metals, Y rare metals
- Provides: X power storage
- Jobs: 0

## Solar Array

Generates power from solar activity

- Cost: X common metals, Y rare metals
- Provides: X power (when sunny)
- Jobs: 1 technician

## Fusion Reactor

Generates power from Hydrogen fusion

- Cost: X common metals, Y rare metals
- Provides: X power
- Jobs: 1 technician, 1 specialist

## Laboratory

Generates research

- Cost: X common metals
- Provides: research (over time)
- Jobs: 3 specialists
