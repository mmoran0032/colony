# Colonists

Each colonist is of a specific type. Colonists can be retrained to change their type, but that takes time and effort from a colonist of that new type.

*Note:* I think to start, we just treat all colonists as colonists/technicians, and build this part up when we want additional complexity. So basically, your colonists are interchangeable.

## Technician

Basic type all colonists start as. Can work standard jobs.

## Specialist

Requires specialist training by a technician. Can work specialist and standard jobs. Standard jobs worked at decreased efficiency. If working in a standard job for too long, will lose specialist status.

## Administrator

Requires administrative training by a technician. Can work administrative jobs and standard jobs. Standard jobs worked at decreased efficiency. If working in a standard job for too long, will lose administrator status.
