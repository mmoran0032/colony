# ARCTURUS

A small colony management simulator

## Inspiration

- Manor Lords: https://en.wikipedia.org/wiki/Manor_Lords
- Orion: https://www.ticalc.org/archives/files/fileinfo/99/9991.html
- Outpost: https://en.wikipedia.org/wiki/Outpost_(1994_video_game)
- Outpost 2: https://en.wikipedia.org/wiki/Outpost_2:_Divided_Destiny
- Race for the Galaxy: https://en.wikipedia.org/wiki/Race_for_the_Galaxy
- Sid Meier's Alpha Centauri: https://en.wikipedia.org/wiki/Sid_Meier%27s_Alpha_Centauri
- Stellaris: https://en.wikipedia.org/wiki/Stellaris_(video_game)
