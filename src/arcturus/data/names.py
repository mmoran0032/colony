"""Names

Used to persist data about various names of things. For the most part, these names will
be assigned randomly when an object requiring a name is created. The player can choose
to change some of the names if they want.

Colonist names

General first names for our colonists. We should bring in names from the following
sources:
- Astronauts, cosmonauts, taikonauts, etc.
- Scientists
- Explorers
- Politicians

In general, try to have an even mix of male/female/either names. In game, there is no
concept of gender, so this is purely aesthetic.

Star names

Use random "Greek letter - constellation" constructions. Override the random constructed
name with the real star name if it exists (e.g., Alpha Canis Minoris -> Procyon). We can
also limit ourselves to a small collection of star systems, based on what is close to
Earth. For instance, we could create "Barnard's Star" as a viable system. That will
probably be a better way to go in the future, but we should still retain the possibility
of generating a random star system.

Which is easier? Generating a random star system should have constraints so that it
seems physical, which might take a while. For nearby star systems, we probably already
know what's in the system, so we just need to pull that information down.

Planet names

Follow current planetary naming conventions based on the star system name.
"""

import random

FEMALE_NAMES = [
    "Anna",
    "Bridget",
    "Dorothy",
    "Eve",
    "Fiona",
    "Grace",
]
MALE_NAMES = [
    "Allen",
    "Brian",
    "Finn",
    "Kenny",
    "Michael",
    "Owen",
]
NAMES = [*FEMALE_NAMES, *MALE_NAMES]


def generate_colonist_name() -> str:
    return random.choice(NAMES)


# GREEK_LOWER_CASE = [chr(i) for i in range(945, 970)]
GREEK_LOWER_CASE = [chr(i) for i in range(945, 947)]  # TEMP to get alpha/beta only
CONTELLATION_NAMES = [
    "Andromedae",
    "Antliae",
    "Apodis",
    "Aquarii",
    "Aquilae",
    "Arae",
    "Arietis",
    "Aurigae",
    "Boötis",
    "Caeli",
    "Camelopardalis",
    "Cancri",
    "Canum Venaticorum",
    "Canis Majoris",
    "Canis Minoris",
    "Capricorni",
    "Carinae",
    "Cassiopeiae",
    "Centauri",
    "Cephei",
    "Ceti",
    "Chamaeleontis",
    "Circini",
    "Columbae",
    "Comae Berenices",
    "Coronae Australis",
    "Coronae Borealis",
    "Corvi",
    "Crateris",
    "Crucis",
    "Cygni",
    "Delphini",
    "Doradus",
    "Draconis",
    "Equulei",
    "Eridani",
    "Fornacis",
    "Geminorum",
    "Gruis",
    "Herculis",
    "Horologii",
    "Hydrae",
    "Hydri",
    "Indi",
    "Lacertae",
    "Leonis",
    "Leonis Minoris",
    "Leporis",
    "Librae",
    "Lupi",
    "Lyncis",
    "Lyrae",
    "Mensae",
    "Microscopiae",
    "Monocerotis",
    "Muscae",
    "Normae",
    "Octanis",
    "Ophiuchi",
    "Orionis",
    "Pavonis",
    "Pegasi",
    "Persei",
    "Phoenicis",
    "Pictoris",
    "Piscium",
    "Piscis Austrini",
    "Puppis",
    "Pyxidis",
    "Reticuli",
    "Sagittae",
    "Sagittarii",
    "Scorpii",
    "Sculptoris",
    "Scuti",
    "Serpentis",
    "Sextantis",
    "Tauri",
    "Telescopii",
    "Trianguli",
    "Tianguli Australis",
    "Tucanae",
    "Ursae Majoris",
    "Ursae Minoris",
    "Velorum",
    "Virginis",
    "Volantis",
    "Vulpeculae",
]
KNOWN_STARS = {
    "α Andromedea": "Alpheratz",
    "α Aquarii": "Sadalsuud",
    "α Aquilae": "Altair",
    "α Arietis": "Hamal",
    "α Aurigae": "Capella",
    "α Boötis": "Arcturus",
    "α Canum Venaticorum": "Cor Caroli",
    "α Canis Majoris": "Siris",
    "α Canis Minoris": "Procyon",
    "α Capricorni": "Deneb Algeni",
    "α Carinae": "Canopus",
    "α Cassiopeiae": "Schedar",
    "α Centauri": "Alpha Centauri",
    "α Cephei": "Alderamin",
    "α Ceti": "Diphda",
    "α Columbae": "Phact",
    "α Coronae Borealis": "Alphecca",
    "α Corvi": "Gienah",
    "α Crucis": "Acrux",
    "α Cygni": "Deneb",
    "α Draconis": "Eltanin",
    "α Eridani": "Archernar",
    "β Geminorum": "Pollux",  # Pollux is actually brighter than Castor
    "α Geminorum": "Castor",
    "α Gruis": "Alnair",
    "α Herculis": "Kornephoros",
    "α Hydrae": "Alphard",
    "α Leonis": "Regulus",
    "α Leporis": "Arneb",
    "α Librae": "Zubeneschemali",
    "α Lyrae": "Vega",
    "α Ophiuchi": "Rasalhague",
    "β Orionis": "Rigel",  # Rigel is actually brighter than Betelgeuse
    "α Orionis": "Betelgeuse",
    "α Pavonis": "Peacock",
    "α Pegasi": "Enif",
    "α Persei": "Mirfak",
    "α Phoenicis": "Ankaa",
}

# see https://en.wikipedia.org/wiki/List_of_nearest_stars for additional details
CLOSE_STARS = [
    "Sirius",
    "Procyon",
    "Epsilon Eridani",
    "Tau Ceti",
    "Alpha Centauri",
    "Barnard's Star",
]


def generate_star_system_name() -> str:
    # name = f"{random.choice(GREEK_LOWER_CASE)} {random.choice(CONTELLATION_NAMES)}"
    # if name in KNOWN_STARS:
    #     return f"{KNOWN_STARS[name]} ({name})"
    name = random.choice(CLOSE_STARS)
    return name
