from pydantic import BaseModel


class Colony(BaseModel):
    colonists: int = 5
    assigned_colonists: int = 0
