from pathlib import Path
from time import monotonic

from textual.app import App, ComposeResult
from textual.containers import HorizontalGroup, VerticalGroup
from textual.reactive import reactive
from textual.screen import Screen
from textual.widgets import Button, Footer, Static

from arcturus.model.colony import Colony

colony = Colony()


class WorkerSlot(Static):
    is_occupied = reactive(False)

    def on_mount(self) -> None:
        self.update(" ")

    def on_click(self) -> None:
        if self.is_occupied:
            self.remove_class("occupied")
            self.is_occupied = False
            colony.assigned_colonists -= 1
        else:
            if colony.assigned_colonists != colony.colonists:
                self.add_class("occupied")
                self.is_occupied = True
                colony.assigned_colonists += 1


class Module(Static):
    def compose(self) -> ComposeResult:
        yield VerticalGroup(
            Static("Module"),
            HorizontalGroup(WorkerSlot(), WorkerSlot(), WorkerSlot()),
        )


DEFAULT_COOLDOWN = 2


class Resupply(VerticalGroup):
    start_time = reactive(monotonic)
    time = reactive(0.0)
    cooldown = reactive(DEFAULT_COOLDOWN)

    def compose(self) -> ComposeResult:
        yield Button("Resupply", id="button", variant="primary")
        yield Static("", id="cooldown")

    def on_button_pressed(self, event: Button.Pressed) -> None:
        event.button.disabled = True
        self.start_time = monotonic()
        self.query_one("#cooldown").update(f"Resupply available in {self.cooldown}")
        self.update_timer = self.set_interval(1 / 60, self.update_time)

    def update_time(self) -> None:
        self.time = monotonic() - self.start_time
        self.cooldown = DEFAULT_COOLDOWN - self.time
        self.query_one("#cooldown").update(f"Resupply available in {self.cooldown}")
        if self.cooldown < 0:
            self.query_one("#button").disabled = False
            self.query_one("#cooldown").update("")
            self.cooldown = DEFAULT_COOLDOWN


class Colony(Screen):
    def compose(self) -> ComposeResult:
        yield HorizontalGroup(
            VerticalGroup(Module(), Module(), Module()),
            Resupply(),
        )
        yield Footer()


class Arcturus(App):
    CSS_PATH = Path(__file__).parent / "arcturus.tcss"
    ENABLE_COMMAND_PALETTE = False  # NB: I probably want to override with my own commands, or set up some other system. This should be fine for now though.
    SCREENS = {"colony": Colony}

    def on_mount(self) -> None:
        self.theme = "textual-dark"
        self.push_screen("colony")
